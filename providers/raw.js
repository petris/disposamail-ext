var regexXOriginalTo = /^X-Original-To: ([^@]+@disposamail.net)$/i;
var regexTo = /^To: ([^@]+@disposamail.net)$/i;
var regexTo2 = /^To: .*<([^@]+@disposamail.net)>.*$/i;

var process_item = function(req, res, process_func) {
    console.log('[Raw] Processing incoming email');
    
    if (!req.body) {
        console.log('[Raw] Missing body');
        return res.status(400).send('Missing body');
    }

    var lines = req.body.split("\n");
    var rcptTo = null;

    for (var i = 0; i < lines.length; i++) {
        var line = lines[i];

        if (line.match(regexXOriginalTo)) {
            rcptTo = line.replace(regexXOriginalTo, "$1");
            break;
        }

        if (line.match(regexTo)) {
            rcptTo = line.replace(regexTo, "$1");
            break;
        }

        if (line.match(regexTo2)) {
            rcptTo = line.replace(regexTo2, "$1");
            break;
        }
    }

    if (!rcptTo) {
        console.log('[Raw] Could not find To address');
        return res.status(400).send('Could not find To address');
    }

    console.log('[Raw] Start processing of email for ' + rcptTo);
        
    process_func(rcptTo, req.body, function() {
        console.log('[Raw] Finished processing of email for ' + rcptTo);
    });

    return res.send('Message delivered');
};

module.exports = {
    process: process_item
}
